# Proyecto
Se ha realizado un proyecto de prueba de concepto en el que se ha implementado un sistema de gestión de roles.

Se ha generado la estructura de base de datos con sus respectivas relaciones, con la finalidad de poder adaptar esta a un sistema de gestión de permisos / roles. En esta prueba de concepto se expone solo un servicio ``getPermissionCatalog``, el cual para un determinado usuario se obtiene el catálogo de permisos en los diferentes módulos de la app (es un mero ejemplo).

Si se quisiera adaptar a un sistema real, la obtención del usuario se obtendría a partir del token del usuario logado, devolviendo el catálogo de permisos en los diferentes módulos para que la parte cliente sepa "pintar" y dar visibilidad de estos módulos. 
Además habría que realizar una comprobación a través de un *midleware* la posible autorizaón de realización de las diferentes llamadas a la api a partir del rol de usuario.

# Instalación

### Dependencias
Instalación de las dependencias del proyecto ``composer install``

### Migraciones
Para realizar correctamente las migraciones realizarlas en el siguiente orden:
1. Role, Module, Operation
2. User, Action
3. RoleAction

### Seeders
Para realizar la siembra de datos para tener una prueba de concepto seguir: ``php artisan db:seeder``, se lanzará ``DatabaseSeeder.php`` el cual ya tiene el orden establecido para realizar la insercción de los datos para la prueba de concepto.

### Diagrama de estructura de base de datos
El concepto relacional de la prueba se basa en:
  + Existen diferentes roles a nivel de aplicación los cuales están asociados a los usuarios.
  + Los roles tienen asignados una serie de acciones.
  + Cada una de las acciones son relacionadas a módulos de aplicación y operaciones que se pueden realizar en dichos módulos.
![Diagrama de base de datos](./permission-diagram.png)