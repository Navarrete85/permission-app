<?php

namespace App\Models;

use App\Models\Operation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class RoleAction extends Model
{
    use HasFactory;

    protected $table = 'role_action';
    protected $primaryKey = 'id_role_action';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id_role_action',
        'id_role',
        'id_action',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return HasOne
     */
    public function role(): HasOne
    {
        return $this->hasOne(Role::class, 'id_role', 'id_role');
    }

    /**
     * @return HasOne
     */
    public function action(): HasOne
    {
        return $this->hasOne(Action::class, 'id_action', 'id_action');
    }
}
