<?php

namespace App\Models;

use App\Models\Module;
use App\Models\Operation;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Action extends Model
{
    use HasFactory;

    protected $table = 'action';
    protected $primaryKey = 'id_action';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id_action',
        'id_module',
        'id_operation',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at',
    ];

    /**
     * @return HasOne
     */
    public function module(): HasOne
    {
        return $this->hasOne(Module::class, 'id_module', 'id_module');
    }

    /**
     * @return HasOne
     */
    public function operation(): HasOne
    {
        return $this->hasOne(Operation::class, 'id_operation', 'id_operation');
    }

    /**
     * @return HasMany
     */
    public function roleActions(): HasMany
    {
        return $this->hasMany(Action::class, 'id_action', 'id_action');
    }
}
