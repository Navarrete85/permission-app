<?php

namespace App\Models;

use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Factories\HasFactory;
// use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Model
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $primaryKey = 'id_user';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'id_user',
        'name',
        'email',
        'password',
        'id_role',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    
    /**
     * @return HasOne
     */
    public function role(): HasOne
    {
        return $this->hasOne(Role::class, 'id_role', 'id_role');
    }


    public function getPermissionCatalog($user_id): array
    {
        $result = [];
        $user = self::where('id_user', $user_id)
            ->with('role.roleActions.action.module')
            ->with('role.roleActions.action.operation')
            ->first()
            ->toArray();
        
        if ($user != null && count($user['role']['role_actions']) > 0) {
            $result['role'] = $user['role']['name'];
            $result['modules'] = [];

            foreach ($user['role']['role_actions'] as $roleAction) {
                $module = $roleAction['action']['module'];
                $operation = $roleAction['action']['operation'];
                
                if (!isset($result['modules'][$module['name']])) {
                    $result['modules'][$module['name']] = [
                        'module' => $module['name'],
                        'longDescr' => $module['long_descr'],
                        'allowOpperations' => []
                    ];
                }

                $result['modules'][$module['name']]['allowOpperations'][] = [
                    'name' => $operation['name'],
                    'longDescr' => $operation['long_descr']
                ];

            }
        }

        return $result;
    }

}
