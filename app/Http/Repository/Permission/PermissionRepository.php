<?php

namespace App\Http\Repository\Permission;

use App\Http\Repository\Permission\IPermissionRepository;
use App\Models\User;

class PermissionRepository implements IPermissionRepository
{

    protected $model;

    /**
     * 
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function data(int $user_id): array {
        return $this->model->getPermissionCatalog($user_id);
    }
}
