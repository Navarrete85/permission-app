<?php

namespace App\Http\Repository\Permission;

use App\Models\User;

interface IPermissionRepository
{
    public function data(int $user_id): array;
}