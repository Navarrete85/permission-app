<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Repository\Permission\PermissionRepository;

class PermissionController extends Controller
{

    private PermissionRepository $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function getPermissionCatalog(Request $request): JsonResponse 
    {
        try {
            // dd($this->permissionRepository);
            $result = $this->permissionRepository->data($request['user_id']);
            return response()->json([
                'message' => 'ok',
                'error' => false,
                'code' => 200,
                'info' => $result
            ], 200);

        } catch (Exception $ex) {
            return $this->error($ex->getMessage());
        }
    }

    public function hola(): string 
    {
        try {

            return 'hola mundo';

        } catch (Exception $ex) {
            return $this->error($ex->getMessage());
        }
    }

}
