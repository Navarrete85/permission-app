<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoleActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('role_action', function (Blueprint $table) {
            $table->id('id_role_action');
            $table->foreignId('id_role');
            $table->foreignId('id_action');
            $table->timestamps();
        });

        Schema::table('role_action', function (Blueprint $table) {
            $table->foreign('id_role')
                ->references('id_role')
                ->on('role');
        });

        Schema::table('role_action', function (Blueprint $table) {
            $table->foreign('id_action')
                ->references('id_action')
                ->on('action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('role_action');
    }
}
