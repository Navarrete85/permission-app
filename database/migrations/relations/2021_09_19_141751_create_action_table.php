<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action', function (Blueprint $table) {
            $table->id('id_action');
            $table->foreignId('id_module');
            $table->foreignId('id_operation');
            $table->timestamps();
        });

        Schema::table('action', function (Blueprint $table) {
            $table->foreign('id_module')
                ->references('id_module')
                ->on('module');
        });

        Schema::table('action', function (Blueprint $table) {
            $table->foreign('id_operation')
                ->references('id_operation')
                ->on('operation');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action');
    }
}
