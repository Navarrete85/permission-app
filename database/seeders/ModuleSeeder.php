<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('module')->insert([
            [
                'name' => 'PermissionModule',
                'long_descr' => 'Módulo para la gestión de los roles / permisos',
            ],
            [
                'name' => 'DateModule',
                'long_descr' => 'Módulo para la gestión de los roles / permisos',
            ],
            [
                'name' => 'SpecialtiesModule',
                'long_descr' => 'Módulo para la gestión de los roles / permisos',
            ],
        ]);
    }
}
