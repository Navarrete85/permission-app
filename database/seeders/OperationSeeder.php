<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OperationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('operation')->insert([
            [
                'name' => 'Visible',
                'long_descr' => 'Ver el contenido',
            ],
            [
                'name' => 'AddNewContent',
                'long_descr' => 'Añadir elementos',
            ],
            [
                'name' => 'Update',
                'long_descr' => 'Actualización',
            ],
            [
                'name' => 'Delete',
                'long_descr' => 'Eliminación de contenido',
            ],
            [
                'name' => 'ChangeDates',
                'long_descr' => 'Actualización de citas',
            ],
        ]);
    }
}
