<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('action')->insert([
            [
                'id_module' => 1,
                'id_operation' => 1,
            ],
            [
                'id_module' => 1,
                'id_operation' => 2,
            ],
            [
                'id_module' => 1,
                'id_operation' => 3,
            ],
            [
                'id_module' => 1,
                'id_operation' => 4,
            ],
            [
                'id_module' => 1,
                'id_operation' => 5,
            ],
            

            [
                'id_module' => 2,
                'id_operation' => 1,
            ],
            [
                'id_module' => 2,
                'id_operation' => 2,
            ],
            [
                'id_module' => 2,
                'id_operation' => 3,
            ],
            [
                'id_module' => 2,
                'id_operation' => 4,
            ],
            [
                'id_module' => 2,
                'id_operation' => 5,
            ],

            
            [
                'id_module' => 3,
                'id_operation' => 1,
            ],
            [
                'id_module' => 3,
                'id_operation' => 2,
            ],
            [
                'id_module' => 3,
                'id_operation' => 3,
            ],
            [
                'id_module' => 3,
                'id_operation' => 4,
            ],
            [
                'id_module' => 3,
                'id_operation' => 5,
            ],
        ]);
    }
}
