<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleActionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('role_action')->insert([
            [
                'id_role' => 1,
                'id_action' => 1,
            ],
            [
                'id_role' => 1,
                'id_action' => 2,
            ],
            [
                'id_role' => 1,
                'id_action' => 3,
            ],
            [
                'id_role' => 1,
                'id_action' => 4,
            ],
            [
                'id_role' => 1,
                'id_action' => 5,
            ],
            [
                'id_role' => 1,
                'id_action' => 6,
            ],
            [
                'id_role' => 1,
                'id_action' => 7,
            ],
            [
                'id_role' => 1,
                'id_action' => 8,
            ],
            [
                'id_role' => 1,
                'id_action' => 9,
            ],
            [
                'id_role' => 1,
                'id_action' => 10,
            ],
            [
                'id_role' => 1,
                'id_action' => 11,
            ],
            [
                'id_role' => 1,
                'id_action' => 12,
            ],
            [
                'id_role' => 1,
                'id_action' => 13,
            ],
            [
                'id_role' => 1,
                'id_action' => 14,
            ],
            [
                'id_role' => 1,
                'id_action' => 15,
            ],
            
            



            [
                'id_role' => 2,
                'id_action' => 1,
            ],
            [
                'id_role' => 2,
                'id_action' => 2,
            ],
            [
                'id_role' => 2,
                'id_action' => 3,
            ],
            [
                'id_role' => 2,
                'id_action' => 7,
            ],
            [
                'id_role' => 2,
                'id_action' => 8,
            ],
            [
                'id_role' => 2,
                'id_action' => 9,
            ],
            [
                'id_role' => 2,
                'id_action' => 13,
            ],
            [
                'id_role' => 2,
                'id_action' => 14,
            ],
            [
                'id_role' => 2,
                'id_action' => 15,
            ],





            [
                'id_role' => 3,
                'id_action' => 1,
            ],
            [
                'id_role' => 3,
                'id_action' => 2,
            ],
            [
                'id_role' => 3,
                'id_action' => 3,
            ],
            [
                'id_role' => 3,
                'id_action' => 4,
            ],
            [
                'id_role' => 3,
                'id_action' => 7,
            ],
        ]);
    }
}
